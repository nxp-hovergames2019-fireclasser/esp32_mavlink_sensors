#include "Spectroscope.h"
#include "ChemH2Ethanol.h"
#include "ChemNO2NH3CO.h"
#include "ChemNDIRCO2.h"
#include "PixyHatDetector.h"
#include <Wire.h>
#include "MavLinkSensors.h"
#include "RapidIotDisplay.h"

RapidIotDisplay rapidIotDisplay;
MavLinkSensors mavlink;

ChemNDIRCO2 chemNDIRCO2;
Spectroscope spectroscope(14);
ChemH2Ethanol chemH2Ethanol;
ChemNO2NH3CO chemNO2NH3CO;
PixyHatDetector pixyHatDetector;

bool info = false;

void setup() {
  Serial.begin(115200);

  Wire.begin();
  //Sensor supports I2C speeds up to 400kHz
  //Wire.setClock(400000);

  mavlink.begin();
  //mavlink.request_datastream();

  printInfo("Spectra");
  spectroscope.begin();
  
  printInfo("H2/Ethanol/TVOC");
  chemH2Ethanol.begin(); 
  
  printInfo("NH3/CO/NO2/C3H8/C4H10/CH4");
  chemNO2NH3CO.begin();
  
  printInfo("CO2/SensTemp/RH");
  chemNDIRCO2.begin();

  pixyHatDetector.begin(); 
  rapidIotDisplay.begin(); 
   
  Serial.println();

}

void loop() {
  //mavlink.receive_data();
  mavlink.send_hearbeat();
  mavlink.time_tick();

  //pixyHatDetector.printReadings(); 
  pixyHatDetector.sendData(mavlink, 70);
  
  printInfo("Spectra");
  spectroscope.takeMeasurements();
  spectroscope.sendData(mavlink, 10);
  spectroscope.printReadings(); 
  
  printInfo("H2/Ethanol/TVOC");
  chemH2Ethanol.sendData(mavlink, 30);
  chemH2Ethanol.printReadings(); 
  
  printInfo("NH3/CO/NO2/C3H8/C4H10/CH4");
  chemNO2NH3CO.sendData(mavlink, 40);
  chemNO2NH3CO.printReadings();
  
  printInfo("CO2/SensTemp/RH");
  chemNDIRCO2.sendData(mavlink, 50);
  chemNDIRCO2.printReadings();
  
  mavlink.send_int(1, "Measured", 60);

  rapidIotDisplay.ensureConnected(); 
  rapidIotDisplay.showCrewCount(pixyHatDetector.getCrewCount()); 
  mavlink.proxyDataTo(rapidIotDisplay);

  Serial.println();  
  delay(500);
}

void printInfo(char* infoText) {
  if (!info) return;
  Serial.print("\t");
  Serial.print(infoText);
  Serial.print(",");
}
