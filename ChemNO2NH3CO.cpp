/*
  ChemNO2NH3CO.cpp 
*/
#include "Arduino.h"
#include "ChemNO2NH3CO.h"
#include <Wire.h>
#include "MutichannelGasSensor.h"
#include "MavLinkSensors.h"

ChemNO2NH3CO::ChemNO2NH3CO()
{
}

void ChemNO2NH3CO::begin()
{
    Serial.print("NH3,CO,NO2,C3H8,C4H10,CH4,");
    gas.begin(0x04);//the default I2C address of the slave is 0x04
    gas.powerOn();
    // note: disable version print in contstructor of library
    //Serial.print("Firmware Version = ");
    //Serial.println(gas.getVersion());
}

void ChemNO2NH3CO::printReadings() {
  printReading(gas.measure_NH3());
  printReading(gas.measure_CO());
  printReading(gas.measure_NO2());
  printReading(gas.measure_C3H8());
  printReading(gas.measure_C4H10());
  printReading(gas.measure_CH4());
  // already obtained from specific sensor
  //printReading(gas.measure_H2());
  //printReading(gas.measure_C2H5OH());
  //measure_CO
}

void ChemNO2NH3CO::printReading(float c) {
  if(c<0) c = 0;
  Serial.print(c); Serial.print(",");
}

void ChemNO2NH3CO::sendData(MavLinkSensors &mavlink, uint8_t base_component_id) {
  mavlink.send_float(gas.measure_NH3(), "NH3", base_component_id);
  mavlink.send_float(gas.measure_CO(), "CO", base_component_id+1);
  mavlink.send_float(gas.measure_NO2(), "NO2", base_component_id+2);
  mavlink.send_float(gas.measure_C3H8(), "C3H8", base_component_id+3);
  mavlink.send_float(gas.measure_C4H10(), "C4H10", base_component_id+4);
  mavlink.send_float(gas.measure_CH4(), "CH4", base_component_id+5);
  // already obtained from specific sensor
  //mavlink.send_float(gas.measure_H2(), "H2");
  //mavlink.send_float(gas.measure_C2H5OH(), "C2H5OH");
  //mavlink.send_float(gas.measure_CO(), "CO");
}
