/*
  ChemNDIRCO2.cpp 
*/
#include "Arduino.h"
#include "ChemNDIRCO2.h"
#include <Wire.h>
#include "MavLinkSensors.h"

//Click here to get the library: http://librarymanager/All#SparkFun_SCD30
//https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library/blob/master/src/SparkFun_SCD30_Arduino_Library.h
#include "SparkFun_SCD30_Arduino_Library.h" 

SCD30 airSensor;


uint16_t lastCO2 = 0;
float    lastHumidity  = 0.0f;
float    lastTemperature = 0.0f;

ChemNDIRCO2::ChemNDIRCO2()
{
}

void ChemNDIRCO2::begin()
{
  airSensor.begin(); //This will cause readings to occur every two seconds
  Serial.print("CO2,"); //(ppm),
  Serial.print("SensTemp,"); //(C)
  Serial.print("RH,"); //(%)

// TODO: calibrate from env.
//  void setMeasurementInterval(uint16_t interval);
//  void setAmbientPressure(uint16_t pressure_mbar);
//  void setAltitudeCompensation(uint16_t altitude);
//  void setAutoSelfCalibration(boolean enable);
//  void setForcedRecalibrationFactor(uint16_t concentration);
//  void setTemperatureOffset(float tempOffset);
  
}

void ChemNDIRCO2::printReadings() {
  if (airSensor.dataAvailable()) {
    lastCO2 = airSensor.getCO2();
    lastTemperature = airSensor.getTemperature();
    lastHumidity = airSensor.getHumidity();
  }

  Serial.print(lastCO2);         Serial.print(",");
  Serial.print(lastTemperature); Serial.print(",");
  Serial.print(lastHumidity);    Serial.print(",");
  
}

void ChemNDIRCO2::sendData(MavLinkSensors &mavlink, uint8_t base_component_id) {
  mavlink.send_int(lastCO2, "CO2", base_component_id);
  mavlink.send_float(lastHumidity, "RH", base_component_id+1);
  mavlink.send_float(lastTemperature, "SensTemp", base_component_id+2);
}
  
