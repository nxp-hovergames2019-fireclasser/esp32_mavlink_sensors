/*
  RapidIotDisplay.h 
*/
#ifndef RapidIotDisplay_h
#define RapidIotDisplay_h

#include "Arduino.h"
//#include "MavLinkSensors.h"

class RapidIotDisplay
{
  public:
    RapidIotDisplay();
    void begin();
    void ensureConnected();
    void setLEDColour(uint8_t agent_id);
    void showCrewCount(uint8_t count);
    void showFireAgent(uint8_t agent_id);
    void showFireClass(uint8_t class_id);
    void printReadings();
//    void sendData(MavLinkSensors &mavlink, uint8_t base_component_id);   
  private:
};

#endif
