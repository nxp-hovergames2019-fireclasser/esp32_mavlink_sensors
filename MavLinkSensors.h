#ifndef MavLinkSensors_h
#define MavLinkSensors_h

#include "Arduino.h"
#include <mavlink.h>
#include "RapidIotDisplay.h"

class MavLinkSensors
{
  public:
    MavLinkSensors();
    void begin();
    void send_hearbeat();
    void send_float(float number, char* infoText, uint8_t _component_id);
    void send_int(uint16_t number, char* infoText, uint8_t _component_id);
    void request_datastream();
    void proxyDataTo(RapidIotDisplay &rapidIotDisplay);
    void time_tick();
  private:
};

#endif
