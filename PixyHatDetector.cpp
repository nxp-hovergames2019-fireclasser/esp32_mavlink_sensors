/*
  PixyHatDetector.cpp 
*/

#include "Arduino.h"
#include "PixyHatDetector.h"

//
//https://docs.pixycam.com/wiki/doku.php?id=wiki:v2:porting_guide#i2c
//https://docs.pixycam.com/wiki/lib/exe/detail.php?id=wiki%3Av2%3Aporting_guide&media=wiki:v2:image_248_2.jpg
//
// begin license header
//
// This file is part of Pixy CMUcam5 or "Pixy" for short
//
// All Pixy source code is provided under the terms of the
// GNU General Public License v2 (http://www.gnu.org/licenses/gpl-2.0.html).
// Those wishing to use Pixy source code, software and/or
// technologies under different licensing terms should contact us at
// cmucam@cs.cmu.edu. Such licensing terms are available for
// all portions of the Pixy codebase presented here.
//
// end license header
//
// This sketch is a good place to start if you're just getting started with 
// Pixy and Arduino.  This program simply prints the detected object blocks 
// (including color codes) through the serial console.  It uses the Arduino's 
// ICSP SPI port.  For more information go here:
//
// https://docs.pixycam.com/wiki/doku.php?id=wiki:v2:hooking_up_pixy_to_a_microcontroller_-28like_an_arduino-29
//

// Uncomment one of these to enable another type of serial interface
#define I2C
//#define UART
//#define SPI_SS
   
#ifdef I2C

#include <Pixy2I2C.h>
Pixy2I2C pixy;

#else 
#ifdef UART

#include <Pixy2UART.h>
Pixy2UART pixy;

#else 
#ifdef SPI_SS

#include <Pixy2SPI_SS.h>
Pixy2SPI_SS pixy;

#else

#include <Pixy2.h>
Pixy2 pixy;

#endif
#endif
#endif

#include <Wire.h>
//#include "MavLinkSensors.h"


PixyHatDetector::PixyHatDetector() { }

void PixyHatDetector::begin() {
  pixy.init();
}

void PixyHatDetector::printReadings() {
  int i; 
  // grab blocks!
  pixy.ccc.getBlocks();
  
  // If there are detect blocks, print them!
  if (pixy.ccc.numBlocks)
  {
    Serial.print("Detected ");
    Serial.println(pixy.ccc.numBlocks);
    for (i=0; i<pixy.ccc.numBlocks; i++)
    {
      Serial.print("  block ");
      Serial.print(i);
      Serial.print(": ");
      //      pixy.ccc.blocks[i].print(); // print all      
      Serial.print(pixy.ccc.blocks[i].m_signature);
      Serial.print(" => ");
      Serial.print(pixy.ccc.blocks[i].m_x);
      Serial.print("/");
      Serial.print(pixy.ccc.blocks[i].m_y);
      Serial.println();
      //  uint16_t m_signature;
      //  uint16_t m_x;
      //  uint16_t m_y;
      //  uint16_t m_width;
      //  uint16_t m_height;
      //  int16_t m_angle;
      //  uint8_t m_index;
      //  uint8_t m_age;
      
      //pixy.blocks[i].signature The signature number of the detected object (1-7 for normal signatures)
      //pixy.blocks[i].x The x location of the center of the detected object (0 to 319)
      //pixy.blocks[i].y The y location of the center of the detected object (0 to 199)
      //pixy.blocks[i].width The width of the detected object (1 to 320)
      //pixy.blocks[i].height The height of the detected object (1 to 200)
      //pixy.blocks[i].angle The angle of the object detected object if the detected object is a color code.
      //pixy.blocks[i].print() A member function that prints the detected object information to the serial port
    }
  }
}

void PixyHatDetector::sendData(MavLinkSensors &mavlink, uint8_t base_component_id) {
  pixy.ccc.getBlocks();
  int crewMembersDetected = 0;
  if (pixy.ccc.numBlocks) crewMembersDetected = pixy.ccc.numBlocks;
  mavlink.send_int(crewMembersDetected, "FireCrew", base_component_id);
}

uint8_t PixyHatDetector::getCrewCount() {
  pixy.ccc.getBlocks();
  int crewMembersDetected = 0;
  if (pixy.ccc.numBlocks) crewMembersDetected = pixy.ccc.numBlocks;
  return crewMembersDetected;
}

    
