/*
  PixyHatDetector.h - Library for flashing PixyHatDetector code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef PixyHatDetector_h
#define PixyHatDetector_h

#include "Arduino.h"
#include "MavLinkSensors.h"

class PixyHatDetector
{
  public:
    PixyHatDetector();
    void begin();
    void printReadings();
    void sendData(MavLinkSensors &mavlink, uint8_t base_component_id);   
    uint8_t getCrewCount();
  private:
};

#endif
