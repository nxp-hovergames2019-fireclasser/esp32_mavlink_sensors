/*
  ChemNDIRCO2.h 
*/
#ifndef ChemNDIRCO2_h
#define ChemNDIRCO2_h

#include "Arduino.h"
#include "MavLinkSensors.h"

class ChemNDIRCO2
{
  public:
    ChemNDIRCO2();
    void begin();
    void printReadings();
    void sendData(MavLinkSensors &mavlink, uint8_t base_component_id);   
  private:
};

#endif
