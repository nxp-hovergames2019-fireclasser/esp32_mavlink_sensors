/*
  ChemNO2NH3CO.h - Library for flashing ChemNO2NH3CO code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef ChemNO2NH3CO_h
#define ChemNO2NH3CO_h

#include "Arduino.h"
#include "MavLinkSensors.h"

class ChemNO2NH3CO
{
  public:
    ChemNO2NH3CO();
    void begin();
    void printReadings();
    void printReading(float c);
    void sendData(MavLinkSensors &mavlink, uint8_t base_component_id);   
  private:
};

#endif
