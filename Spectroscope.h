/*
  Spectroscope.h - Library for flashing Spectroscope code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef Spectroscope_h
#define Spectroscope_h

#include "Arduino.h"
#include "MavLinkSensors.h"

class Spectroscope
{
  public:
    Spectroscope(int pin);
    void begin();
    void takeMeasurements();
    void printReadings();
    void sendData(MavLinkSensors &mavlink, uint8_t base_component_id);
    float getCalibrated410nm();
    float getCalibrated435nm();
    float getCalibrated460nm();
    float getCalibrated485nm();
    float getCalibrated510nm();
    float getCalibrated535nm();
    float getCalibrated560nm();
    float getCalibrated585nm();
    float getCalibrated610nm();
    float getCalibrated645nm();
    float getCalibrated680nm();
    float getCalibrated705nm();
    float getCalibrated730nm();
    float getCalibrated760nm();
    float getCalibrated810nm();
    float getCalibrated860nm();
    float getCalibrated900nm();
    float getCalibrated940nm();    
  private:    
};

#endif
