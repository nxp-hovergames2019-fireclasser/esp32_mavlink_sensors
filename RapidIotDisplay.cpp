/*
  RapidIotDisplay.cpp 
*/

#include "Arduino.h"
#include "RapidIotDisplay.h"
#include <Wire.h>
//#include "MavLinkSensors.h"

#include "BLEDevice.h"
//#include "BLEScan.h"

BLEAddress  nxpDeviceAddress = BLEAddress("00:60:37:12:7a:99");

bool rapidIOTdebug = false;

// The remote service we wish to connect to.
static BLEUUID serviceUUID("21b02d34-c2e7-4584-9480-42eb845be111");
// The characteristic of the remote service we are interested in.
static BLEUUID    charUUID1("21b02d34-c2e7-4584-9480-42eb84111111"); // #1
static BLEUUID    charUUID2("21b02d34-c2e7-4584-9480-42eb84111222"); // #2
static BLEUUID    charUUID3("21b02d34-c2e7-4584-9480-42eb84111333"); // #3
static BLEUUID    charUUID4("21b02d34-c2e7-4584-9480-42eb84111444"); // #4

static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;
static BLERemoteCharacteristic* pRemoteCharacteristic1;
static BLERemoteCharacteristic* pRemoteCharacteristic2;
static BLERemoteCharacteristic* pRemoteCharacteristic3;
static BLERemoteCharacteristic* pRemoteCharacteristic4;
static BLEAdvertisedDevice* myDevice;

static void notifyCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify) {
    if (rapidIOTdebug) {
      Serial.print("Notify callback for characteristic ");
      Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
      Serial.print(" of data length ");
      Serial.println(length);
      Serial.print("data: ");
      Serial.println((char*)pData);
    }
}

class MyClientCallback : public BLEClientCallbacks {
  void onConnect(BLEClient* pclient) {
  }

  void onDisconnect(BLEClient* pclient) {
    connected = false;
    if (rapidIOTdebug) {
      Serial.println("onDisconnect");
    }
  }
};

bool connectToServer() {
    if (rapidIOTdebug) {
      Serial.print("Forming a connection to ");
      Serial.println(myDevice->getAddress().toString().c_str());
    }
    
    BLEClient*  pClient  = BLEDevice::createClient();
    
    if (rapidIOTdebug) { Serial.println(" - Created client"); }

    pClient->setClientCallbacks(new MyClientCallback());

    // Connect to the remove BLE Server.
    pClient->connect(myDevice);  // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)
    if (rapidIOTdebug) { Serial.println(" - Connected to server"); }

    // Obtain a reference to the service we are after in the remote BLE server.
    BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
    if (pRemoteService == nullptr) {
      if (rapidIOTdebug) {
        Serial.print("Failed to find our service UUID: ");
        Serial.println(serviceUUID.toString().c_str());
      }
      pClient->disconnect();
      return false;
    }
    if (rapidIOTdebug) { Serial.println(" - Found our service"); }


    // Obtain a reference to the characteristic in the service of the remote BLE server.
    pRemoteCharacteristic1 = pRemoteService->getCharacteristic(charUUID1);
    if (pRemoteCharacteristic1 == nullptr) {
      if (rapidIOTdebug) {
        Serial.print("Failed to find our characteristic UUID: ");
        Serial.println(charUUID1.toString().c_str());
      }
      pClient->disconnect();
      return false;
    }
    if (rapidIOTdebug) { Serial.println(" - Found our characteristic 1"); }

    pRemoteCharacteristic2 = pRemoteService->getCharacteristic(charUUID2);
    if (pRemoteCharacteristic2 == nullptr) {
      if (rapidIOTdebug) { 
        Serial.print("Failed to find our characteristic UUID: ");
        Serial.println(charUUID2.toString().c_str());
      }
      pClient->disconnect();
      return false;
    }
    if (rapidIOTdebug) { Serial.println(" - Found our characteristic 2"); }

    pRemoteCharacteristic3 = pRemoteService->getCharacteristic(charUUID3);
    if (pRemoteCharacteristic3 == nullptr) {
      if (rapidIOTdebug) { 
        Serial.print("Failed to find our characteristic UUID: ");
        Serial.println(charUUID3.toString().c_str());
      }
      pClient->disconnect();
      return false;
    }
    if (rapidIOTdebug) { Serial.println(" - Found our characteristic 3"); }

    pRemoteCharacteristic4 = pRemoteService->getCharacteristic(charUUID4);
    if (pRemoteCharacteristic4 == nullptr) {
      if (rapidIOTdebug) { 
        Serial.print("Failed to find our characteristic UUID: ");
        Serial.println(charUUID4.toString().c_str());
      }
      pClient->disconnect();
      return false;
    }
    if (rapidIOTdebug) { Serial.println(" - Found our characteristic 4"); }

    connected = true;
}

/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
 /**
   * Called for each advertising BLE server.
   */
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    if (rapidIOTdebug) { 
      Serial.print("BLE Advertised Device found: ");
      Serial.println(advertisedDevice.toString().c_str());
    }
    BLEAddress  discoverdAddress = advertisedDevice.getAddress();
    //Serial.println(discoverdAddress.toString().c_str());
    //Serial.println(nxpDeviceAddress.toString().c_str());

    // We have found a device, let us now see if it contains the service we are looking for.
    //if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {
    if (nxpDeviceAddress.equals(discoverdAddress)) {
      if (rapidIOTdebug) { 
        Serial.println();
        Serial.println("Detected our device...yay...");
      }
      BLEDevice::getScan()->stop();
      myDevice = new BLEAdvertisedDevice(advertisedDevice);
      doConnect = true;
      doScan = true;

    } // Found our server
  } // onResult
}; // MyAdvertisedDeviceCallbacks


RapidIotDisplay::RapidIotDisplay()
{
}

void RapidIotDisplay::begin() {
  BLEDevice::init("");

  // Retrieve a Scanner and set the callback we want to use to be informed when we
  // have detected a new device.  Specify that we want active scanning and start the
  // scan to run for 5 seconds.
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);
  pBLEScan->start(5, false);
}

void RapidIotDisplay::ensureConnected() {
  // If the flag "doConnect" is true then we have scanned for and found the desired
  // BLE Server with which we wish to connect.  Now we connect to it.  Once we are 
  // connected we set the connected flag to be true.
  if (doConnect == true) {
    if (connectToServer()) {
      if (rapidIOTdebug) { Serial.println("We are now connected to the BLE Server."); }
    } else {
      if (rapidIOTdebug) { Serial.println("We have failed to connect to the server; there is nothin more we will do."); }
    }
    doConnect = false;
  }

  // If we are connected to a peer BLE Server, update the characteristic each time we are reached
  // with the current time since boot.
  if (connected) {
    // do nothing (processed later)
  }else if(doScan){
    BLEDevice::getScan()->start(0);  // this is just eample to start scan after disconnect, most likely there is better way to do it in arduino
  } 
}

  

  
 


void RapidIotDisplay::setLEDColour(uint8_t agent_id) {
  if (connected) {
    String colours[] = {
      "0x00ff00", // green - nothing detected
      "0xff0000", // red
      "0xEEBD19", // cream 
      "0x0000ff", // blue
      "0xffffff", // black -> white
      "0xffff00", // yellow
    };
    String colour = colours[agent_id%6];
    pRemoteCharacteristic4->writeValue(colour.c_str(), colour.length());
  }  
}

void RapidIotDisplay::showCrewCount(uint8_t count) {
  if (connected) {
    String newValue = "Crew: " + String(count);
    pRemoteCharacteristic3->writeValue(newValue.c_str(), newValue.length());
  }  
}

void RapidIotDisplay::showFireClass(uint8_t class_id) {
  if (connected) {
    String fireClasses[] = {
      "not detected", // green - nothing detected
      "Solids", // red
      "Liquids", // cream 
      "Gases", // blue
      "Metals", // blue
      "Electricals", // black -> white
      "Oils", // yellow
    };
    String fireClass = fireClasses[class_id%7];
    String newValue = "" + fireClass;
    if (rapidIOTdebug) { Serial.println("Setting new characteristic value to \"" + newValue + "\""); }
    pRemoteCharacteristic1->writeValue(newValue.c_str(), newValue.length());
  }  
}

void RapidIotDisplay::showFireAgent(uint8_t agent_id) {
  if (connected) {
    String fireAgents[] = {
      "not detected", // green - nothing detected
      "Water",   // red
      "Foam",    // cream 
      "Powder",  // blue
      "CO2",     // black -> white
      "Wet Chemicals", // yellow
    };
    String fireAgent = fireAgents[agent_id%6];
    String newValue = "" + fireAgent;
    if (rapidIOTdebug) { Serial.println("Setting new characteristic value to \"" + newValue + "\""); }
    pRemoteCharacteristic2->writeValue(newValue.c_str(), newValue.length());
  }  
}

//
//void RapidIotDisplay::sendData(MavLinkSensors &mavlink, uint8_t base_component_id) {
//  mavlink.send_int(mySensor.H2, "H2", base_component_id);
//  mavlink.send_int(mySensor.ethanol, "Ethanol", base_component_id+1);
//  mavlink.send_int(mySensor.TVOC, "TVOC", base_component_id+2);
//}
