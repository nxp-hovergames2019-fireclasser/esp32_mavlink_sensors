/*
  Spectroscope.cpp - Library for flashing Spectroscope code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
  https://www.arduino.cc/en/Hacking/LibraryTutorial
*/

#include "Arduino.h"
#include "Spectroscope.h"
#include "SparkFun_AS7265X.h" //Click here to get the library: http://librarymanager/All#SparkFun_AS7265X
#include "MavLinkSensors.h"

AS7265X sensor;

Spectroscope::Spectroscope(int pin)
{
}

void Spectroscope::begin()
{
  if(sensor.begin() == false)
  {
    Serial.println("AS7265X not responding");
    while(1);
  }
  sensor.disableIndicator(); //Turn off the blue status LED

  Serial.print("410nm,435nm,460nm,485nm,510nm,535nm,560nm,585nm,610nm,645nm,680nm,705nm,730nm,760nm,810nm,860nm,900nm,940nm,");
  
}

float Spectroscope::getCalibrated410nm() { return (sensor.getCalibratedA()); }
float Spectroscope::getCalibrated435nm() { return (sensor.getCalibratedB()); }
float Spectroscope::getCalibrated460nm() { return (sensor.getCalibratedC()); }
float Spectroscope::getCalibrated485nm() { return (sensor.getCalibratedD()); }
float Spectroscope::getCalibrated510nm() { return (sensor.getCalibratedE()); }
float Spectroscope::getCalibrated535nm() { return (sensor.getCalibratedF()); }
float Spectroscope::getCalibrated560nm() { return (sensor.getCalibratedG()); }
float Spectroscope::getCalibrated585nm() { return (sensor.getCalibratedH()); }
float Spectroscope::getCalibrated610nm() { return (sensor.getCalibratedR()); }
float Spectroscope::getCalibrated645nm() { return (sensor.getCalibratedI()); }
float Spectroscope::getCalibrated680nm() { return (sensor.getCalibratedS()); }
float Spectroscope::getCalibrated705nm() { return (sensor.getCalibratedJ()); }
float Spectroscope::getCalibrated730nm() { return (sensor.getCalibratedT()); }
float Spectroscope::getCalibrated760nm() { return (sensor.getCalibratedU()); }
float Spectroscope::getCalibrated810nm() { return (sensor.getCalibratedV()); }
float Spectroscope::getCalibrated860nm() { return (sensor.getCalibratedW()); }
float Spectroscope::getCalibrated900nm() { return (sensor.getCalibratedK()); }
float Spectroscope::getCalibrated940nm() { return (sensor.getCalibratedL()); }

void Spectroscope::takeMeasurements()
{
  //sensor.takeMeasurements(); //This is a hard wait while all 18 channels are measured
  sensor.takeMeasurementsWithBulb(); //This is a hard wait while all 18 channels are measured
}

void Spectroscope::printReadings() {
  Serial.print(getCalibrated410nm()); Serial.print(",");
  Serial.print(getCalibrated435nm()); Serial.print(",");
  Serial.print(getCalibrated460nm()); Serial.print(",");
  Serial.print(getCalibrated485nm()); Serial.print(",");
  Serial.print(getCalibrated510nm()); Serial.print(",");
  Serial.print(getCalibrated535nm()); Serial.print(",");
  Serial.print(getCalibrated560nm()); Serial.print(",");
  Serial.print(getCalibrated585nm()); Serial.print(",");
  Serial.print(getCalibrated610nm()); Serial.print(",");
  Serial.print(getCalibrated645nm()); Serial.print(",");
  Serial.print(getCalibrated680nm()); Serial.print(",");
  Serial.print(getCalibrated705nm()); Serial.print(",");
  Serial.print(getCalibrated730nm()); Serial.print(",");
  Serial.print(getCalibrated760nm()); Serial.print(",");
  Serial.print(getCalibrated810nm()); Serial.print(",");
  Serial.print(getCalibrated860nm()); Serial.print(",");
  Serial.print(getCalibrated900nm()); Serial.print(",");
  Serial.print(getCalibrated940nm()); Serial.print(",");
}

void Spectroscope::sendData(MavLinkSensors &mavlink, uint8_t base_component_id) {
  mavlink.send_float(getCalibrated410nm(), "410nm", base_component_id);
  mavlink.send_float(getCalibrated435nm(), "435nm", base_component_id+1);
  mavlink.send_float(getCalibrated460nm(), "460nm", base_component_id+2);
  mavlink.send_float(getCalibrated485nm(), "485nm", base_component_id+3);
  mavlink.send_float(getCalibrated510nm(), "510nm", base_component_id+4);
  mavlink.send_float(getCalibrated535nm(), "535nm", base_component_id+5);
  mavlink.send_float(getCalibrated560nm(), "560nm", base_component_id+6);
  mavlink.send_float(getCalibrated585nm(), "585nm", base_component_id+7);
  mavlink.send_float(getCalibrated610nm(), "610nm", base_component_id+8);
  mavlink.send_float(getCalibrated645nm(), "645nm", base_component_id+9);
  mavlink.send_float(getCalibrated680nm(), "680nm", base_component_id+10);
  mavlink.send_float(getCalibrated705nm(), "705nm", base_component_id+11);
  mavlink.send_float(getCalibrated730nm(), "730nm", base_component_id+12);
  mavlink.send_float(getCalibrated760nm(), "760nm", base_component_id+13);
  mavlink.send_float(getCalibrated810nm(), "810nm", base_component_id+14);
  mavlink.send_float(getCalibrated860nm(), "860nm", base_component_id+15);
  mavlink.send_float(getCalibrated900nm(), "900nm", base_component_id+16);
  mavlink.send_float(getCalibrated940nm(), "940nm", base_component_id+17);
}

   
