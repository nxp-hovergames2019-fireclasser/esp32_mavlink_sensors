/*
  ChemH2Ethanol.h - Library for flashing ChemH2Ethanol code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef ChemH2Ethanol_h
#define ChemH2Ethanol_h

#include "Arduino.h"
#include "MavLinkSensors.h"

class ChemH2Ethanol
{
  public:
    ChemH2Ethanol();
    void begin();
    void printReadings();
    void sendData(MavLinkSensors &mavlink, uint8_t base_component_id);   
  private:
};

#endif
