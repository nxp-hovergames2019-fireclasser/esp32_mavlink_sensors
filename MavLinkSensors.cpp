#include "Arduino.h"
#include "MavLinkSensors.h"
#include <mavlink.h>

uint64_t time_usec = 4; // TODO: obtain time
uint8_t _system_id = 254; // id of computer which is sending the command (ground control software has id of 255)

MavLinkSensors::MavLinkSensors() {
}

void MavLinkSensors::begin() {
  Serial2.begin(57600); // port 16,17
}

void MavLinkSensors::time_tick() {
  time_usec = time_usec + 1;
}

void MavLinkSensors::send_hearbeat() {
  //Serial.println("Sending Heartbeats...");
  uint8_t _component_id = 158; // seems like it can be any # except the number of what Pixhawk sys_id is
  mavlink_message_t msghb;
  mavlink_heartbeat_t heartbeat;
  uint8_t bufhb[MAVLINK_MAX_PACKET_LEN];
  mavlink_msg_heartbeat_pack(_system_id, _component_id, &msghb, MAV_TYPE_QUADROTOR, MAV_AUTOPILOT_INVALID, MAV_MODE_PREFLIGHT, 0, MAV_STATE_STANDBY);
  uint16_t lenhb = mavlink_msg_to_send_buffer(bufhb, &msghb);
  Serial2.write(bufhb, lenhb);
  //Serial.println("Heartbeats sent");
}

void MavLinkSensors::send_float(float number, char* infoText, uint8_t _component_id) {
  //Serial.print("Sending Float... @ ");
  //Serial.print(number);
  mavlink_message_t msghb;
  uint8_t bufhb[MAVLINK_MAX_PACKET_LEN];
  mavlink_msg_named_value_float_pack(_system_id, _component_id, &msghb, time_usec, infoText, number);
  uint16_t lenhb = mavlink_msg_to_send_buffer(bufhb, &msghb);
  Serial2.write(bufhb, lenhb);
  //Serial.println("Float sent");
}

void MavLinkSensors::send_int(uint16_t number, char* infoText, uint8_t _component_id) {
  //Serial.print("Sending uint16_t... @ ");
  //Serial.print(number);
  mavlink_message_t msghb;
  uint8_t bufhb[MAVLINK_MAX_PACKET_LEN];
  mavlink_msg_named_value_int_pack(_system_id, _component_id, &msghb, time_usec, infoText, number);
  uint16_t lenhb = mavlink_msg_to_send_buffer(bufhb, &msghb);
  Serial2.write(bufhb, lenhb);
  //Serial.println("uint16_t sent");
}

void MavLinkSensors::request_datastream() {
  //Request Data from Pixhawk
  uint8_t _system_id = 254; // id of computer which is sending the command (ground control software has id of 255)
  uint8_t _component_id = 2; // seems like it can be any # except the number of what Pixhawk sys_id is
  uint8_t _target_system = 1; // Id # of Pixhawk (should be 1)
  uint8_t _target_component = 0; // Target component, 0 = all (seems to work with 0 or 1
  uint8_t _req_stream_id = MAV_DATA_STREAM_ALL;
  uint16_t _req_message_rate = 0x01; //number of times per second to request the data in hex
  uint8_t _start_stop = 1; //1 = start, 0 = stop

  // STREAMS that can be requested
  /*
     Definitions are in common.h: enum MAV_DATA_STREAM and more importantly at:
     https://mavlink.io/en/messages/common.html#MAV_DATA_STREAM

     MAV_DATA_STREAM_ALL=0, // Enable all data streams
     MAV_DATA_STREAM_RAW_SENSORS=1, /* Enable IMU_RAW, GPS_RAW, GPS_STATUS packets.
     MAV_DATA_STREAM_EXTENDED_STATUS=2, /* Enable GPS_STATUS, CONTROL_STATUS, AUX_STATUS
     MAV_DATA_STREAM_RC_CHANNELS=3, /* Enable RC_CHANNELS_SCALED, RC_CHANNELS_RAW, SERVO_OUTPUT_RAW
     MAV_DATA_STREAM_RAW_CONTROLLER=4, /* Enable ATTITUDE_CONTROLLER_OUTPUT, POSITION_CONTROLLER_OUTPUT, NAV_CONTROLLER_OUTPUT.
     MAV_DATA_STREAM_POSITION=6, /* Enable LOCAL_POSITION, GLOBAL_POSITION/GLOBAL_POSITION_INT messages.
     MAV_DATA_STREAM_EXTRA1=10, /* Dependent on the autopilot
     MAV_DATA_STREAM_EXTRA2=11, /* Dependent on the autopilot
     MAV_DATA_STREAM_EXTRA3=12, /* Dependent on the autopilot
     MAV_DATA_STREAM_ENUM_END=13,

     Data in PixHawk available in:
      - Battery, amperage and voltage (SYS_STATUS) in MAV_DATA_STREAM_EXTENDED_STATUS
      - Gyro info (IMU_SCALED) in MAV_DATA_STREAM_EXTRA1
  */

  // Initialize the required buffers
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];

  // Pack the message
  mavlink_msg_request_data_stream_pack(_system_id, _component_id, &msg, _target_system, _target_component, _req_stream_id, _req_message_rate, _start_stop);
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);  // Send the message (.write sends as bytes)

  Serial2.write(buf, len); //Write data to serial port
}


//function called by arduino to read any MAVlink messages sent by serial communication from flight controller to arduino
void MavLinkSensors::proxyDataTo(RapidIotDisplay &rapidIotDisplay) {
  mavlink_message_t msg;
  mavlink_status_t status;

  while (Serial2.available()) {
    uint8_t c = Serial2.read();
    //Get new message
    if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status)) {
      //Serial.printf("Received message with ID %d, sequence: %d from component %d of system %d\n", msg.msgid, msg.seq, msg.compid, msg.sysid);

      //Handle new message from autopilot
      switch (msg.msgid) {
         case MAVLINK_MSG_ID_NAMED_VALUE_INT: {
            mavlink_named_value_int_t packet;
            mavlink_msg_named_value_int_decode(&msg, &packet);

            if (strcmp(packet.name,"FireClass")==0) {
//              Serial.print("NAMED_VALUE_INT name:");     Serial.print(packet.name);
//              Serial.print(":  value:  ");     Serial.print(packet.value);
//              Serial.println();  
              rapidIotDisplay.showFireClass(packet.value);              
            }
  
             if (strcmp(packet.name,"FireAgent")==0) {
//              Serial.print("NAMED_VALUE_INT name:");     Serial.print(packet.name);
//              Serial.print(":  value:  ");     Serial.print(packet.value);
//              Serial.println();  
              rapidIotDisplay.setLEDColour(packet.value); 
              rapidIotDisplay.showFireAgent(packet.value);
             }
          }
          break;          
      }
    }
  }
}
