/*
  ChemH2Ethanol.cpp 

https://github.com/sparkfun/SparkFun_SGP30_Arduino_Library/blob/master/src/SparkFun_SGP30_Arduino_Library.h

*/

#include "Arduino.h"
#include "ChemH2Ethanol.h"
#include "SparkFun_SGP30_Arduino_Library.h" // Click here to get the library: http://librarymanager/All#SparkFun_SGP30
#include <Wire.h>
#include "MavLinkSensors.h"

SGP30 mySensor; //create an object of the SGP30 class
long t1, t2;

ChemH2Ethanol::ChemH2Ethanol()
{
}

void ChemH2Ethanol::begin()
{
  //Initialize sensor
  if (mySensor.begin() == false) {
    Serial.println("No SGP30 Detected. Check connections.");
    while (1);
  }
  //Initializes sensor for air quality readings
  //measureAirQuality should be called in one second increments after a call to initAirQuality
  mySensor.initAirQuality();
  t1 = millis();

  Serial.print("H2,Ethanol,TVOC,"); 
}

void ChemH2Ethanol::printReadings()
{
  //First fifteen readings will be
  //CO2: 400 ppm  TVOC: 0 ppb
  // TODO: ignore them

  //TODO: add humidity compensation
  
  t2 = millis();
  if ( t2 >= t1 + 1000) {
    t1 = t2;
    //measure CO2 and TVOC levels
    mySensor.measureAirQuality();
//    Serial.print("CO2: ");  Serial.print(mySensor.CO2);  Serial.print(" ppm\t");
//    Serial.print("TVOC: "); Serial.print(mySensor.TVOC); Serial.print(" ppb\t");
    mySensor.measureRawSignals();
  } 

  // print always
  Serial.print(mySensor.H2);      Serial.print(","); //uint16_t
  Serial.print(mySensor.ethanol); Serial.print(","); //uint16_t
  Serial.print(mySensor.TVOC);    Serial.print(","); //uint16_t
}

void ChemH2Ethanol::sendData(MavLinkSensors &mavlink, uint8_t base_component_id) {
  mavlink.send_int(mySensor.H2, "H2", base_component_id);
  mavlink.send_int(mySensor.ethanol, "Ethanol", base_component_id+1);
  mavlink.send_int(mySensor.TVOC, "TVOC", base_component_id+2);
}
